#!/bin/sh

BOARD_DIR="$(dirname $0)"
BOARD_NAME="$(basename ${BOARD_DIR})"
GENIMAGE_CFG="${BOARD_DIR}/genimage-${BOARD_NAME}.cfg"
GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"
MKIMAGE=$HOST_DIR/usr/bin/mkimage
BOOT_CMD=$BOARD_DIR/boot.scr
BOOT_CMD_H=$BINARIES_DIR/boot.scr.uimg
UPDATE_CMD=$BOARD_DIR/update.scr
UPDATE_CMD_H=$BINARIES_DIR/update.scr.uimg

# U-Boot script
if [ -e $MKIMAGE -a -e $BOOT_CMD ];
then
	$MKIMAGE -A arm -O linux -T script -C none -d $BOOT_CMD $BOOT_CMD_H
fi

# U-Boot script
if [ -e $MKIMAGE -a -e $UPDATE_CMD ];
then
	$MKIMAGE -A arm -O linux -T script -C none -d $UPDATE_CMD $UPDATE_CMD_H
fi

# Mark the kernel as DT-enabled
mkdir -p "${BINARIES_DIR}/kernel-marked"
${HOST_DIR}/usr/bin/mkknlimg "${BINARIES_DIR}/zImage" \
	"${BINARIES_DIR}/kernel-marked/zImage"

rm -rf "${GENIMAGE_TMP}"

genimage                           \
	--rootpath "${TARGET_DIR}"     \
	--tmppath "${GENIMAGE_TMP}"    \
	--inputpath "${BINARIES_DIR}"  \
	--outputpath "${BINARIES_DIR}" \
	--config "${GENIMAGE_CFG}"

exit $?
